const logsHandlers = require('./handlers/logs');
const fileHandlers = require('./handlers/files');
const { defaultHandler } = require('./handlers/default');

/**
 * @param {'GET' | 'POST'} method 
 * @param {string} path 
 * @returns {(req, res) => Promise<{
 *                        status: number, 
 *                        eventType: string, 
 *                        message: string, 
 *                        contentType: string}>}
 */
function getRequestHandler(method, path) {
    const pathWithNoQuery = path.split('?')[0];
    if (method === 'GET') {
        if (isSimpleRouteWithParam(pathWithNoQuery, ['file'])) {
            return fileHandlers.onGetFile; 
        }
        if (isSimpleRoute(pathWithNoQuery, ['logs'])) {
            return logsHandlers.onGetLogs;
        }
        return defaultHandler;
    } else if (method === 'POST') {
        if (isSimpleRoute(pathWithNoQuery, ['file'])) {
            return fileHandlers.onCreateFile; 
        }
        return defaultHandler;
    } 
    return defaultHandler;
}

/**
 * checks if path is like `path/to/resourse`
 * @param {string} _path 
 * @param {Array<string>} routeParts 
 * @returns {boolean}
 */
function isSimpleRoute(_path, routeParts) {
    const pathParts = clearPathPartsFromEmptyStrings(_path.split('/'));
    return pathParts.length === routeParts.length &&
           pathParts.every((part, i) => part === routeParts[i]);
}

/**
 * checks if path is like `path/to/resourse/:id`
 * @param {string} _path 
 * @param {Array<string>} routeParts 
 * @returns {boolean}
 */
function isSimpleRouteWithParam(_path, routeParts) {
    const pathParts = clearPathPartsFromEmptyStrings(_path.split('/'));
    return pathParts.length - 1 === routeParts.length &&
           pathParts.every((part, i) => part === routeParts[i] || i === routeParts.length);
}


/**
 * @param {Array<string>} pathParts 
 * @returns {Array<string>}
 */
function clearPathPartsFromEmptyStrings(pathParts) {
    return pathParts.filter(part => !!part);
}

module.exports = {
    getRequestHandler,
};