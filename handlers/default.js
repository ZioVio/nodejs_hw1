const { eventTypeEnum } = require('../services/logs/logs.service');

/**
 * @param {Request} req 
 * @param {Response} res 
 */
async function defaultHandler(req, res) {
    return {
        eventType: eventTypeEnum.PATH_UNKNOWN,
        status: 404,
        message: `No resourse found for required path: '${req.method} ${req.url}'`,
    };
}

module.exports = {
    defaultHandler,
};