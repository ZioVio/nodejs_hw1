const { URL } = require('url');
const { getLogs, eventTypeEnum } = require('../../services/logs/logs.service');
const { handleUnknownError } = require('../error');
const config = require('../../config');

/**
 * @param {Request} req 
 * @param {Response} res 
 */
async function onGetLogs(req, res) {
    const { searchParams } = new URL(req.url, config.host);
    const from = searchParams.get('from');
    const to = searchParams.get('to');
    try {
        const logs = getLogs(from, to);
        return getSuccessGetLogsResponse(logs);
    } catch (err) {
        return handleUnknownError(err, 'Some error occured while reading logs');
    }
}


/**
 * @param {string} filename 
 * @returns {Log}
 */
function getSuccessGetLogsResponse(logs) {
    return {
        eventType: eventTypeEnum.LOGS_GET_SUCCESS,
        logMessage: 'Logs returned with no errors',
        message: JSON.stringify(logs),
        status: 200,
        contentType: 'application/json',
    };
}



module.exports = {
    onGetLogs,
};