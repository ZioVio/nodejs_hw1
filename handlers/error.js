const { eventTypeEnum } = require('../services/logs/logs.service');

/**
 * @param {Error} err 
 * @param {string} displayMessage 
 * @returns {Log}
 */
function handleUnknownError(err, displayMessage = 'Unknown error occured') {
    return {
        eventType: eventTypeEnum.ERR_UNKNOWN,
        message: displayMessage,
        logMessage: err.message,
        status: 500,
    };
}

module.exports = {
    handleUnknownError,
};