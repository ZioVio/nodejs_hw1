const { URL } = require('url');
const { getFile } = require('../../services/files/files.service');
const { eventTypeEnum } = require('../../services/logs/logs.service');
const { handleUnknownError } = require('../error');
const config = require('../../config');


/**
 * @param {Request} req 
 * @param {Response} res 
 */
async function onGetFile(req, res) {
    const { pathname } = new URL(req.url, config.host);
    try {
        const content = await getFile(pathname);
        if (!content) {
            return getNotFoundFileResponse(pathname);
        }
        return getSuccessGetFileResponse(pathname, content);
    } catch (err) {
        return handleUnknownError(err, 'Some error occured while reading a file');
    }
}

/**
 * @param {string} filename 
 * @param {string} content 
 * @returns {Log}
 */
function getSuccessGetFileResponse(filename, content) {
    return {
        eventType: eventTypeEnum.FILE_GET_SUCCESS,
        status: 200,
        message: content, 
        logMessage: `File '${filename}' returned with no errors`,
    };
}


/**
 * @param {string} filename  
 * @returns {Log}
 */
function getNotFoundFileResponse(filename) {
    return {
        eventType: eventTypeEnum.FILE_GET_ERROR,
        status: 400,
        message: `No file for path '${filename}' found`, 
    };
}

module.exports = {
    onGetFile,
};