const { URL } = require('url');
const { saveFile } = require('../../services/files/files.service');
const { MaximumFileSizeExeededError } = require('../../entities/errors/MaximumFileSizeExeededError');
const { eventTypeEnum } = require('../../services/logs/logs.service');
const { handleUnknownError } = require('../error');
const config = require('../../config');

/**
 * @param {Request} req 
 * @param {Response} res 
 */
async function onCreateFile(req, res) {
    try {
        const { searchParams } = new URL(req.url, config.host);
        const filename = searchParams.get('filename');
        const content = searchParams.get('content');
        if (!filename || !content) {
            return getEmptyInputResponse();
        }
        const contentLength = Buffer.from(content).length;
        const newFilename = await saveFile(filename, content, contentLength);
        return getSuccessCreateFileResponse(newFilename);
       
    } catch (err) {
        return handleCreateFileError(err);
    }
}


/**
 * Handles possible errors for creating file
 * @param {Error | MaximumFileSizeExeededError} err 
 * @returns {Log}
 */
function handleCreateFileError(err) {
    if (err instanceof MaximumFileSizeExeededError) {
        return {
            eventType: eventTypeEnum.FILE_WRITE_ERROR,
            message: err.message,
            status: 400,
        };
    } 
    return handleUnknownError(err, 'Some error occured while creating a file');
}


/**
 * @param {string} filename 
 * @returns {Log}
 */
function getSuccessCreateFileResponse(filename) {
    return {
        eventType: eventTypeEnum.FILE_WRITE_SUCCESS,
        message: `File '${filename}' was saved`,
        status: 200,
    };
}

/**
 * @returns {Log}
 */
function getEmptyInputResponse() {
    return {
        eventType: eventTypeEnum.FILE_WRITE_ERROR,
        message: 'Filename or content is empty',
        status: 400,
    };
}

module.exports = {
    onCreateFile,
};