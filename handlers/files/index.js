const { onCreateFile } = require('./createFile');
const { onGetFile } = require('./getFile');
module.exports = {
    onCreateFile,
    onGetFile,
};