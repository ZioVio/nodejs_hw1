/**
 * @extends Error
 */
class MaximumFileSizeExeededError extends Error {

    constructor(size, maxSize) {
        super(
            `Maximum file size exeeded (${maxSize / 1024}kb). Your file size is ${size / 1024}kb`
        );
    }

}

module.exports = {
    MaximumFileSizeExeededError,
};