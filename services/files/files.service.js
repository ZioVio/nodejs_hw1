const fs = require('fs').promises;
const path = require('path');
const config = require('../../config');
const { MaximumFileSizeExeededError } = require('../../entities/errors/MaximumFileSizeExeededError');

/**
 * 
 * @param {string} filename 
 * @param {string} content 
 * @param {number} contentBytesLength
 * @returns {string} filename if file is saved or throws `MaximumFileSizeExeededError` if content size is exeeded or other error occured
 */
async function saveFile(filename, content, contentBytesLength) {
    if (contentBytesLength > config.maxFileSize) {
        throw new MaximumFileSizeExeededError(contentBytesLength, config.maxFileSize);
    }
    const baseFilename = path.basename(filename);
    const fullpath = path.join(config.storagePath, baseFilename);
    await fs.writeFile(fullpath, content, config.encoding);
    return baseFilename;
}

/**
 * 
 * @param {string} path 
 * @returns {string} file content or `null` if no file found
 */
async function getFile(filename) {
    filename = path.basename(filename);
    try {
        const content = await fs.readFile(path.join(config.storagePath, filename), config.encoding);
        return content;
    } catch (err) {
        return null;
    }
}


module.exports = {
    saveFile,
    getFile,
};