const config = require('../../config');
const fs = require('fs');

/**
 * @returns {Array<Log>} 
 */
function loadLogsSync() {
    const rawLogs = fs.readFileSync(config.logsPath);
    const { logs } = JSON.parse(rawLogs.toString());
    if (!Array.isArray(logs)) {
        return [];
    }
    return logs;
}

/**
 * Checks if the date is valid
 * @param {Date} date 
 */
function isValidDate(date) {
    return date instanceof Date && !isNaN(date);
}

/**
 * Universal method which creates Date object from 3 possible inputs
 * @param {Date | string | number} input 
 * @returns {Date}
 */
function createDateFromStringOrNumberOrDate(input) {
    if (input === null || input === undefined) {
        // invalid date
        return new Date('');
    }
    return !isNaN(+input) ? new Date(+input) : new Date(input);
}

module.exports = {
    loadLogsSync,
    isValidDate,
    createDateFromStringOrNumberOrDate,
};