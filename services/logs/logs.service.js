const fs = require('fs').promises;
const config = require('../../config');
const { loadLogsSync, isValidDate, createDateFromStringOrNumberOrDate } = require('./utils');

const eventTypeEnum = {
    FILE_WRITE_SUCCESS: 'FILE_WRITE_SUCCESS',
    FILE_WRITE_ERROR:   'FILE_WRITE_ERROR',

    FILE_GET_SUCCESS:   'FILE_GET_SUCCESS', 
    FILE_GET_ERROR:     'FILE_GET_ERROR',

    LOGS_GET_SUCCESS:   'LOGS_GET_SUCCESS',
    LOGS_GET_ERROR:     'LOGS_GET_ERROR',

    ERR_UNKNOWN:        'ERR_UNKNOWN',
    PATH_UNKNOWN:       'PATH_UNKNOWN',
};

const logs = loadLogsSync();

/**
 * @param {keyof(eventTypeEnum)} eventType
 * @param {string} message
 * @returns {Promise<{
 * eventType: keyof(eventTypeEnum),
 * logMessage: string,
 * timestamp: number,
 * status: number
 * }>}
 */
async function log(eventType, message, status) {
    while (logs.length >= config.maxLogsSize) {
        logs.shift();
    }
    
    const log = {
        eventType,
        message,
        status,
        timestamp: Date.now(),
    };

    logs.push(log);
    await fs.writeFile(config.logsPath, JSON.stringify({ logs }, null, 2), config.encoding);
    return log;
}

/**
 * Returns the logs in the specified date range. If any of the params is null or invalid date it is ignored
 * @param {Date | string | number} from 
 * @param {Date | string | number} to 
 * @returns {Log}
 */
function getLogs(from = null, to = null) {
    const parsedFrom = createDateFromStringOrNumberOrDate(from);
    const parsedTo = createDateFromStringOrNumberOrDate(to);
    const isDateFromValid = isValidDate(parsedFrom);
    const isDateToValid = isValidDate(parsedTo);
    
    if (!isDateFromValid && !isDateToValid) {
        return logs;
    }
    
    if (isDateFromValid && isDateToValid) {
        return logs.filter(log => log.timestamp >= parsedFrom.getTime() && 
                                  log.timestamp <= parsedTo.getTime());
    }
    
    if (isDateFromValid) {
        return logs.filter(log => log.timestamp >= parsedFrom.getTime());
    }
    
    return logs.filter(log => log.timestamp <= parsedTo.getTime());
}

module.exports = {
    eventTypeEnum, 
    log,
    getLogs,
};