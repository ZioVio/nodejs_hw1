const http = require('http');
const { log } = require('./services/logs/logs.service');
const config = require('./config');
const { getRequestHandler } = require('./routesResolver');


module.exports = () => {
    http.createServer((req, res) => {
        const handler = getRequestHandler(req.method.toUpperCase(), req.url);
        handler(req, res).then(({ status, message, eventType, contentType, logMessage }) => {
            res.writeHead(status, { 'Content-type': contentType ? contentType : 'text/html' });
            res.end(message);
            log(eventType, logMessage || message)
                .then(() => {})
                .catch((err) => {
                    console.log('Logging error:', err);
                    // handle logging error, can ignore for this task
                });
        });
    })
    .listen(config.port)
    .on('listening', () => console.log(`Server started on port ${config.port}`))
    .on('error', (err) => console.error(err.message));
};



