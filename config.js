const path = require('path');

module.exports = {
    maxFileSize: 1024 * 2, // 2kb
    maxLogsSize: 500,
    encoding: 'utf-8',
    logsPath: path.join(__dirname, 'logs/logs.json'),
    storagePath: path.join(__dirname, 'storage'),
    port: process.env.PORT || 4000,
    host: 'http://host/',
};